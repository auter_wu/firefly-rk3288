
/* AudioStreamInALSA.cpp
 **
 ** Copyright 2008-2009 Wind River Systems
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **
 **     http://www.apache.org/licenses/LICENSE-2.0
 **
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 */

#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>

#define LOG_TAG "AudioHardwareALSA"
#include <utils/Log.h>
#include <utils/String8.h>

#include <cutils/properties.h>
#include <media/AudioRecord.h>
#include <hardware_legacy/power.h>

#include "AudioHardwareALSA.h"

namespace android_audio_legacy
{

AudioStreamInALSA::AudioStreamInALSA(AudioHardwareALSA *parent,
        alsa_handle_t *handle,
        AudioSystem::audio_in_acoustics audio_acoustics) :
    ALSAStreamOps(parent, handle),
    mFramesLost(0),
    mAcoustics(audio_acoustics),
    mMicMute(false)
{
    acoustic_device_t *aDev = acoustics();

    if (aDev) aDev->set_params(aDev, mAcoustics, NULL);

#if (SPEEX_AGC_ENABLE||SPEEX_DENOISE_ENABLE)
    if(!mParent->mAudioPreprocessing)
        return;

    mSpeexFrameSize = int(mHandle->periodSize>>1);
    mSpeexPcmIn = new int16_t[mSpeexFrameSize];
    mSpeexState = speex_preprocess_state_init(mSpeexFrameSize, mHandle->sampleRate);
#if SPEEX_AGC_ENABLE
    int agc = 1;
    float  q= mParent->mAgcLevel; //q<=30000
    //actually default is 8000(0,32768),here make it louder for voice is not loudy enough by default. 8000
    speex_preprocess_ctl(mSpeexState, SPEEX_PREPROCESS_SET_AGC, &agc);
    speex_preprocess_ctl(mSpeexState, SPEEX_PREPROCESS_SET_AGC_LEVEL,&q);
#endif//SPEEX_AGC_ENABLE

#if SPEEX_DENOISE_ENABLE
    int denoise = 1;
#if SPEEX_AGC_ENABLE
    int noiseSuppress = mParent->mNsLevel;//db: -25~-45
#else
    int noiseSuppress = mParent->mNsLevel;
#endif    
    speex_preprocess_ctl(mSpeexState, SPEEX_PREPROCESS_SET_DENOISE, &denoise);
    speex_preprocess_ctl(mSpeexState, SPEEX_PREPROCESS_SET_NOISE_SUPPRESS, &noiseSuppress);
#endif//SPEEX_DENOISE_ENABLE

#endif//SPEEX_AGC_ENABLE||SPEEX_DENOISE_ENABLE

#if DEBUG_PCMFILE
	prop_pcm = 0;
#endif
}

AudioStreamInALSA::~AudioStreamInALSA()
{
#if (SPEEX_AGC_ENABLE||SPEEX_DENOISE_ENABLE)
    if(!mParent->mAudioPreprocessing)
        return;

    speex_preprocess_state_destroy(mSpeexState);
    if(mSpeexPcmIn)
        delete[] mSpeexPcmIn;
#endif //SPEEX_AGC_ENABLE||SPEEX_DENOISE_ENABLE

    close();
}

status_t AudioStreamInALSA::setGain(float gain)
{
	if(gain == 0.0)
		mMicMute= true;
	else
		mMicMute = false;
	
	return NO_ERROR; 

    //return mixer() ? mixer()->setMasterGain(gain) : (status_t)NO_INIT;
}

ssize_t AudioStreamInALSA::read(void *buffer, ssize_t bytes)
{
    android::AutoMutex lock(mLock);

    if (!mPowerLock) {
        acquire_wake_lock (PARTIAL_WAKE_LOCK, "AudioInLock");
        mPowerLock = true;
    }

    acoustic_device_t *aDev = acoustics();

    // If there is an acoustics module read method, then it overrides this
    // implementation (unlike AudioStreamOutALSA write).
    if (aDev && aDev->read)
        return aDev->read(aDev, buffer, bytes);

    snd_pcm_sframes_t n, frames = snd_pcm_bytes_to_frames(mHandle->handle, bytes);
    status_t          err;

    do {
        n = snd_pcm_readi(mHandle->handle, buffer, frames);
        if (n < frames) {
			LOGE("AudioStreamInALSA snd_pcm_readi return = %ld\n",n);
            if (mHandle->handle) {
                if (n < 0) {
                    n = snd_pcm_recover(mHandle->handle, n, 0);

                    if (aDev && aDev->recover) aDev->recover(aDev, n);
                } else
                    n = snd_pcm_prepare(mHandle->handle);
            }
            return static_cast<ssize_t>(n);
        }
    } while (n == -EAGAIN);
#if DEBUG_PCMFILE
	//prop debug
	char value[PROPERTY_VALUE_MAX];
	property_get("media.capture.control", value, NULL);
	prop_pcm = atoi(value);	
	if(prop_pcm)
	{
		static int fd=0;
		static int offset = 0;		
		fd=::open("/data/media/debug_pcmfile",O_WRONLY|O_CREAT,0777);
		if(fd < 0)
		{
			LOGD("DEBUG open /data/media/debug_pcmfile error =%d ,errno = %d",fd,errno);
			prop_pcm = 0;
			offset = 0;
			system("setprop media.capture.control 0");
		}	
		::lseek(fd,offset,SEEK_SET);	
		::write(fd,buffer,snd_pcm_frames_to_bytes(mHandle->handle, n));
		offset += snd_pcm_frames_to_bytes(mHandle->handle, n);
		::close(fd);
		if(offset >= 2*1024*1024)
		{
			prop_pcm = 0;
			offset = 0;
			system("setprop media.capture.control 0");
		//	LOGD("RET = %d",property_set("media.capture.control","0"));
			LOGD("test capture pcmfile end ");
		}
	}
#endif

    if(mMicMute)
        memset(buffer,0,bytes);

#if (SPEEX_AGC_ENABLE||SPEEX_DENOISE_ENABLE)
    do {
        if(!mParent->mAudioPreprocessing)
            break;
		//temply we do not process usb mic
        if(mHandle->curDev == AudioSystem::DEVICE_IN_DGTL_DOCK_HEADSET)
            break;

        if(!mMicMute)
        {
            int index = 0;
            int startPos = 0;
            spx_int16_t* data = (spx_int16_t*) buffer;

            int curFrameSize = int(n);
            //ALOGI("speex preprocessing.");
            if(curFrameSize != 2*mSpeexFrameSize)
                ALOGE("the current request have some error mSpeexFrameSize %d bytes %d ",mSpeexFrameSize,bytes);

            while(curFrameSize >= startPos+mSpeexFrameSize)
            {
                speex_preprocess_run(mSpeexState,(int16_t*)data);
                startPos += mSpeexFrameSize;
                data += mSpeexFrameSize;
            }
        }
    }while(0);
#endif//(SPEEX_AGC_ENABLE||SPEEX_DENOISE_ENABLE)

    return static_cast<ssize_t>(snd_pcm_frames_to_bytes(mHandle->handle, n));
}

status_t AudioStreamInALSA::dump(int fd, const Vector<String16>& args)
{
    return NO_ERROR;
}

status_t AudioStreamInALSA::open(int mode)
{
    android::AutoMutex lock(mLock);

    status_t status = ALSAStreamOps::open(mode);

    acoustic_device_t *aDev = acoustics();

    if (status == NO_ERROR && aDev)
        status = aDev->use_handle(aDev, mHandle);

    return status;
}

status_t AudioStreamInALSA::close()
{
    android::AutoMutex lock(mLock);

    acoustic_device_t *aDev = acoustics();

    if (mHandle && aDev) aDev->cleanup(aDev);

    ALSAStreamOps::close();

    if (mPowerLock) {
        release_wake_lock ("AudioInLock");
        mPowerLock = false;
    }

    return NO_ERROR;
}

status_t AudioStreamInALSA::standby()
{
    android::AutoMutex lock(mLock);

    if (mPowerLock) {
        release_wake_lock ("AudioInLock");
        mPowerLock = false;
    }

    return NO_ERROR;
}

void AudioStreamInALSA::resetFramesLost()
{
    mFramesLost = 0;
}

unsigned int AudioStreamInALSA::getInputFramesLost() const
{
    unsigned int count = mFramesLost;
    // Stupid interface wants us to have a side effect of clearing the count
    // but is defined as a const to prevent such a thing.
    ((AudioStreamInALSA *)this)->resetFramesLost();
    return count;
}

status_t AudioStreamInALSA::setAcousticParams(void *params)
{
    android::AutoMutex lock(mLock);

    acoustic_device_t *aDev = acoustics();

    return aDev ? aDev->set_params(aDev, mAcoustics, params) : (status_t)NO_ERROR;
}

size_t    AudioStreamInALSA::getInputBufferSize(uint32_t sampleRate, int format, int channels)
{
	LOGD("AudioStreamInALSA::getInputBufferSize sampleRate:%d,format:%d,channelCount:%d",
		sampleRate,format,channels);

	size_t ratio;

	switch (sampleRate)
	{	
		case 8000:
		case 11025:
			ratio = 4;
			break;
		case 16000:
		case 22050:
			ratio = 2;
			break;
		case 44100:
		default:
			ratio = 1;
			break;
	}
	
	if (format != AudioSystem::PCM_16_BIT) {
        LOGW("getInputBufferSize bad format: %d", format);
        return 0;
    }
    if (channels<1 || channels>2) {
        LOGW("getInputBufferSize bad channel count: %d", channels);
        return 0;
    }

	return (2048*channels*sizeof(int16_t)) / ratio ;			
}

const uint32_t AudioStreamInALSA::inputSamplingRates[] = {
        8000, 11025, 16000, 22050, 44100
};

uint32_t AudioStreamInALSA::getInputSampleRate(uint32_t sampleRate)
{
    uint32_t i;
    uint32_t prevDelta;
    uint32_t delta;

    for (i = 0, prevDelta = 0xFFFFFFFF; i < sizeof(inputSamplingRates)/sizeof(uint32_t); i++, prevDelta = delta) {
        delta = abs(sampleRate - inputSamplingRates[i]);
        if (delta > prevDelta) break;
    }
    // i is always > 0 here
    return inputSamplingRates[i-1];
}



status_t AudioStreamInALSA::setMicMute(bool state)
{
    //ALOGE("setMicMute(%d) mMicMute %d", state, mMicMute);
    android::AutoMutex lock(mLock);

    if (mMicMute != state) {
          mMicMute = state;
    }

	return NO_ERROR;

}

status_t AudioStreamInALSA::getMicMute(bool *state)
{
	//ALOGE("getMicMute mMicMute %d", mMicMute);
    *state = mMicMute;

	return NO_ERROR;
}

}       // namespace android
